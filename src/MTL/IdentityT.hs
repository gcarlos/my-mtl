module MTL.IdentityT (
    IdentityT(..)
  , someFunc
) where

import MTL.MonadTrans

someFunc = putStrLn "hi"

newtype IdentityT m a = IdentityT {runIdentityT :: m a}

instance MonadTrans IdentityT where
  lift = IdentityT

mapIdentity :: (m a -> n b) -> IdentityT m a -> IdentityT n b
mapIdentity f = IdentityT . f . runIdentityT

lift2IdentityT :: (m a -> n b -> p c) -> IdentityT m a -> IdentityT n b -> IdentityT p c
lift2IdentityT f a b = IdentityT (f (runIdentityT a) (runIdentityT b))

instance (Functor m) => Functor (IdentityT m) where
  fmap f = mapIdentity (fmap f)

instance (Foldable f) => Foldable (IdentityT f) where
  foldMap f (IdentityT a) = foldMap f a

instance (Applicative m) => Applicative (IdentityT m) where
  pure x = IdentityT (pure x)
  (<*>) = lift2IdentityT (<*>)

instance (Monad m) => Monad (IdentityT m) where
  return = IdentityT . return
  m >>= k = IdentityT $ runIdentityT . k =<< runIdentityT m

-- (=<<) :: Monad m => (a -> m b) -> m a -> m b
