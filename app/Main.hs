module Main where
import MTL.IdentityT

main :: IO()
main = someFunc

-- examples
ex1 = Div (Val 10) (Val 2)
ex2 = Div (Val 10) (Val 0)

examples = [ex1, ex2]

-- term
data Term
  = Val Int
  | Div Term Term
  deriving Show

--evaluator
runEval1 :: IdentityT m a -> m a
runEval1 m = runIdentityT m

eval1 :: Monad m => Term -> IdentityT m Int
eval1 (Val a) = return a
eval1 (Div t u) = do v1 <- eval1 t
                     v2 <- eval1 u
                     return (v1 `div` v2)

-- run examples
run1 :: Monad m => m [Int]
run1 = mapM (runEval1 . eval1) examples
